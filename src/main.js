import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import { rtdbPlugin as VueFire } from 'vuefire'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'
export const eventBus = new Vue()


Vue.config.productionTip = false
Vue.use(VueFire)
Vue.use(VueSweetalert2)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app")
